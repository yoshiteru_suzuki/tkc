<?php
require_once('./seminar/2019/news.php');
$today = date("Y-m-d");
$today = new DateTime($today);
?><!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
  <title>漢方店舗専門のコンサルタント Takuya kanpo consulting</title>
  <meta property="og:type" content="website">
  <meta name="description" content="漢方店舗専門のコンサルタント。新宿にて毎月セミナー開催中！">
<?php readfile('./assets/ssi/head.html'); ?>
</head>
<body class="home">
  <div id="wrapper">
<?php readfile('./assets/ssi/header.html'); ?>
  	<div id="contents">
      <section class="cons-list">
        <header class="inner">
          <h2>漢方店舗コンサルタント</h2>
          <h3>漢方取扱店舗のこんなご希望にお応えします。</h3>
        </header>
        <div class="inner">
          <ul>
            <li class="open">
              <a href="/consultant/open.html">
                <span class="icon">漢方取扱店開店</span>
                <span>成功する漢方取扱店を、できるだけお金をかけずにスタートさせたい。</span>
              </a>
            </li>
            <li class="staff">
              <a href="/consultant/staff.html">
                <span class="icon">スタッフ育成</span>
                <span>漢方を売れるスタッフを育成したい。</span>
              </a>
            </li>
            <li class="uriage">
              <a href="/consultant/uriage.html">
                <span class="icon">売上アップ</span>
                <span>とにかく店舗の売上を上げたい。</span>
              </a>
            </li>
            <li class="keishousha">
              <a href="/consultant/keishousha.html">
                <span class="icon">店舗継承</span>
                <span>店舗を継承したい、受けたい。</span>
              </a>
            </li>
          </ul>
        </div>
      </section>
      <section class="semi-list2">
        <div class="wrap">
          <div class="inner">
            <header class="inner">
              <h2>セミナー</h2>
              <h3>オンラインにて毎月セミナー開催中です！</h3>
            </header>
						<div class="wrap">
							<div class="semi-movie">
								<div>
									<iframe width="560" height="315" src="https://www.youtube.com/embed/SOjHfOU_J9I?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
								</div>
			        </div>
							<section class="under-sec semi-news">
								<h2 class="semi-ttl"><span>お知らせ</span></h2>
								<?php $data_count = 0;?>
								<?php foreach($list as $key => $member):?>
									<?php
									$date = str_replace('.','-',$member['date']);
									$date = new DateTime($date);
									 if(($date <= $today) || $_SERVER['HTTP_HOST'] != 'takuya-kanpo-consulting.com'):
										 $data_count++;
										 $new = $member['new'];
									?>
									<dl class="<?php echo $new; ?>">
										<dt><?php echo $member['date'];  ?></dt>
										<dd>
											<p class="fb"><?php echo $member['ttl']; ?></p>
											<?php echo $member['txt']; ?>
										</dd>
									</dl>
								<?php endif; ?>
								<?php endforeach; ?>
							</section>
<h2 class="semi-ttl"><span>セミナー一覧</span><i>受講生募集中</i></h2>
<?php readfile('./seminar/2019/list.html'); ?>
						</div>
          </div>
        </div>
      </section>
    </div>
  </div>
<?php readfile('./assets/ssi/footer.html'); ?>
</body>
</html>

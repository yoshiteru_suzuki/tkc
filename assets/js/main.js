/*
 * 共通設定
 */
if (!window.console){ window.console = { log:function(msg){},debug:function(msg){}}; } //debug setting

var pathn = location.pathname.split('/');

var headerHeight;

$(function(){

	headerHeight = $('#site-header').height();

	setGlobalNavi();
	setGlobalNavCurrent();
	setLoadPageAnchor();

	/*
	 * 読み込みページアンカー
	 */
	function setLoadPageAnchor(){
		$(window).on('load',function(){
			var param = getRequest();
			var href;
			var target;
			var duration = 1000;
			var offset = -headerHeight;
			if(param && param['anchor']){
				setTimeout(function(){
					target = $("#"+param['anchor']);
					target.velocity("scroll", { duration: duration, easing: "swing", offset: offset });
				},100);
			}
		});
	}

	/*
	 * グロナビ
	 */
	function setGlobalNavi(){
	  $btn = $('#global-nav-btn');
	  $body = $('body');
	  $btn.click(function(){
			$body.toggleClass('navopen');
			if ($btn.hasClass('close'))
			  $btn.removeClass('close');
			else
			  $btn.addClass('close');
		});
	}

	/*
	 * グロナビ、カレント設定
	 */
	function setGlobalNavCurrent(){
		var param = location.search,
				$elm =$(".nav-list .list"),
				$this,navreg;

		var path = pathn[2];
		if(pathn[2] == ''){
			path = 'home';
		}

		$elm.each(function()
		{
			$this = $(this);
			if(path == $this.data("nav")){
				$this.addClass('current');
			}
		});
		// if(path == 'home'){
		// 	$('#site-header .womens a, #site-header .staff a').each(function()
		// 	{
		// 		$this = $(this);
		// 		$this.attr('href', $this.attr('href').replace('/icbwomensstyle/\?anchor=','#')).addClass('pageanchor');
		// 	});
		// }
	}
}); // ready

$(window).on('load', function() {
	$('body').addClass("ready");
	setPageAnchorAnimetion();
});

/*
 * ページアンカー
 */
function setPageAnchorAnimetion(){

	var href;
	var target;
	var position;
	var duration = 1000;
	var offset = -headerHeight;

	$('a[href^="#"]').click(function() {
		href= $(this).attr("href");
		target = $(href == "#" || href == "" ? 'html' : href);
		target.velocity("scroll", { duration: duration, easing: "swing", offset: offset });
		return false;
	});
}

function getRequest(){
	if(location.search.length > 1) {
		var get = new Object();
		var ret = location.search.substr(1).split("&");
		for(var i = 0; i < ret.length; i++) {
			var r = ret[i].split("=");
			get[r[0]] = r[1];
		}
		return get;
	} else {
		return false;
	}
}

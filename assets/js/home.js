$(function() {

	var $carouselPic = $('#carousel-pic');
	var carouselLoad = false;

	var carouselPicImages = [];
	$carouselPic.find(".pic").each(function() {
		return carouselPicImages.push($(this).css('background-image').replace('url("','').replace('")',''));
	});

	$.imgpreload(carouselPicImages, {
		all: function() {
			carouselLoad = true;
			loadCarousel();
		}
	});
	setTimeout(function () {
		if(!carouselLoad) loadCarousel();
	},3000);

	function loadCarousel(){
		$('.top').addClass('load');
		$('.top .fade').addClass('show');
		$carouselPic.addClass('load');
		setTimeout(function () {
			setCarouselPic();
		},3000);
	}

	function setCarouselPic(){
		var param = {
			responsive: true,
			items: {
				visible: 1
			},
			scroll: {
				items: 1,
				fx: "crossfade",
				easing: "easeInQuart",
				duration: 2500,
				timeoutDuration: 1500,
			},
			auto: {
				play: true
			},
			onCreate: function() {
				$carouselPic.removeClass('load');
			}
		}
		$carouselPic.carouFredSel(param);
	}
//	$("<script>").attr("src", '/shared/js/min/'+deviceType+'.home.min.js').appendTo("body");
});

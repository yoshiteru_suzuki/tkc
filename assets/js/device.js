var ua = navigator.userAgent.toLowerCase(),
    deviceData = {
      isIOS     : false,
      isAndroid : false,
      isTablet  : false,
      isIE      : false,
      device    : "",
      verString : "",
      version   : 0
    };

if (/iphone|ipod|ipad/.test(ua)) {
  deviceData.isIOS  = true;

  if (/version/.test(ua)) {
    deviceData.verString = /version\/([^\s]+)/.exec(ua)[1];
  }

  if (/iphone/.test(ua)) {
    deviceData.device = "iPhone";
  } else if (/ipad/.test(ua)) {
    deviceData.isTablet = true;
    deviceData.device = "iPad";
  } else if (/ipod/.test(ua)) {
    deviceData.device = "iPod";
  }
} else if (/android/.test(ua)) {
  deviceData.isAndroid = true;
  deviceData.verString  = /android\s([^;]+)/.exec(ua)[1];
  if (/mobile/.test(ua)) {
    deviceData.device = "Android Mobile";
  } else {
    deviceData.isTablet = true;
  }

} else if (ua.indexOf('msie') >= 0 || ua.indexOf('trident') >= 0) {
    deviceData.isIE = true;
    deviceData.verString = ua.appVersion;
}

var deviceType;
if(!deviceData.isTablet && (deviceData.isIOS || deviceData.isAndroid)){
  deviceType = "mobile";
  document.write ('<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">');
} else{
  document.write ('<meta name="viewport" content="width=1100">');
  deviceType = "desktop";
}


var param = getRequest();
if(param && param['device']) deviceType = param['device'];

function getRequest(){
	if(location.search.length > 1) {
		var get = new Object();
		var ret = location.search.substr(1).split("&");
		for(var i = 0; i < ret.length; i++) {
			var r = ret[i].split("=");
			get[r[0]] = r[1];
		}
		return get;
	} else {
		return false;
	}
}

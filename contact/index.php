<?php
define("DEBUG", 0); // 1:debug 0:not debug
define("ENC_INT", 'UTF-8');// 内部文字コード
define("ENC_OUT", 'UTF-8');// 出力文字コード
define("MAIL_FROM_ADDRESS", 'info@takuya-kanpo-consulting.com');
define("MAIL_TO_ADDRESS_ADMIN", 'info@takuya-kanpo-consulting.com');
define("MAIL_FROM_TEXT", 'Takuya kanpo consulting');

ini_set('display_errors', 0);

$naiyou_list = array (
	"漢方店舗コンサルタント",
	"スタッフ育成コンサルタント",
	"店舗売上アップコンサルタント",
	"店舗継承コンサルタント（継承したい）",
	"店舗継承コンサルタント（継承を受けたい）",
	"その他"
);

$txt_list = array (
	"お問い合わせ、お申し込み、ご質問などお気軽にお問い合わせください。",
  "お問い合わせありがとうございます。",
  "お問い合わせを受け付けました。"
);

mb_internal_encoding(ENC_INT);
mb_http_output(ENC_OUT);
mb_language("ja");

//フォームライブラリクラス
require_once("../assets/php/FormLib.php");
$FormLib = new FormLib();

$post = $FormLib->rhe($_POST,mb_internal_encoding());
unset($_POST);
$param = $FormLib->rhe($_GET,mb_internal_encoding());
unset($_GET);


//入力
if(empty($param['act']) || $post['submitback'])
{
	if(!empty($param['test'])){
		require_once("test.php");
	}
	input("");
}
//確認
else if($param['act'] == 1)
{
	if(empty($post)) header("Location: ./");
	confirm($post);
}
//メール送信
else if($param['act'] == 2)
{
	if(empty($post)) header("Location: ./");
	send();
}
//完了画面
else
{
	thanks();
}


function thanks()
{
	global $htmldata;
	$htmldata['TITLE'] = "：送信完了";
	$htmldata['ACTION_CLASS'] = "complete";
	$htmldata['READ_TEXT'] = <<< HTML
	<section class="mailform">
		<div class="complete-txt">
		  <p>お問い合わせありがとうございました。</p>
		  <p>お問い合わせは、通常3営業日以内に担当者よりご連絡いたします。<br>内容によって回答にお時間がかかる場合があります。あらかじめご了承ください。</p>
		</div>
	</section>
HTML;
}

function send()
{
	global $FormLib, $post, $naiyou_list, $txt_list;

	if(isset($post["naiyou_chk"])){
	  foreach((array) $post["naiyou_chk"] as $val){
	  	$naiyou_chk .= $naiyou_list[$val-1]."　";
	  }
	}

	$inputData = <<< DATA
_お名前
{$post['name']}
{$post['name-kana']}


DATA;

if(strlen($post["organization"]) != 0){
$inputData .= <<< DATA
_会社・店舗・団体名
{$post['organization']}


DATA;
}
if(strlen($post["address-level1"].$post["address-level2"].$post["address-line1"]) != 0){
$inputData .= <<< DATA
_住所
{$post['address-level1']}{$post['address-level2']}{$post['address-line1']}


DATA;
}

if(strlen($post["tel"]) != 0){
$inputData .= <<< DATA
_電話番号
{$post['tel']}


DATA;
}

if(strlen($post["fax"]) != 0){
$inputData .= <<< DATA
_FAX番号
{$post['fax']}


DATA;
}

$inputData .= <<< DATA
_メールアドレス
{$post['email']}


DATA;

if(isset($post["naiyou_chk"])){
$inputData .= <<< DATA
_お問い合わせ内容
{$naiyou_chk}

{$post['naiyou']}

DATA;
}else{
$inputData .= <<< DATA
_お問い合わせ内容
{$post['naiyou']}

DATA;
}


	$name = "";
	if(strlen($post["organization"]) != 0){
		$name = $post["organization"]." ";
	}
	$name .= $post['name']."様";

	// 送信者へ
	$body = <<< BODY
{$name}

{$txt_list[1]}
以下の内容でお問い合わせを受け付けいたしました。
通常3営業日以内に担当者よりご連絡いたします。
内容によって回答にお時間がかかる場合があります。あらかじめご了承ください。


■ご送信内容の確認

{$inputData}

------------------------------------------------------
Takuya kanpo consulting
https://takuya-kanpo-consulting.com/

（漢方のスギヤマ薬局 本店内）
〒 252-0001 座間市相模が丘5-10-37
TEL: 042-746-1951
------------------------------------------------------
BODY;

	$subject = "[Takuya kanpo consulting] お問い合わせ（自動返信メール）";
	$ret = $FormLib->sendMail($post['email'] , $subject , $body, MAIL_FROM_TEXT, MAIL_FROM_ADDRESS, DEBUG);


	// 管理者へ
	$body = <<< BODY
{$txt_list[2]}

{$inputData}

BODY;

	$subject = "[Takuya kanpo consulting] お問い合わせ";

	if(DEBUG)
		$ret .= $FormLib->sendMail($post['email'] , $subject , $body, MAIL_FROM_TEXT, $post['email'], DEBUG);
	else
		$ret .= $FormLib->sendMail(MAIL_TO_ADDRESS_ADMIN , $subject , $body, MAIL_FROM_TEXT, $post['email'], DEBUG);

	header("Location: ./?act=3");
}


function confirm($post)
{
	global $htmldata, $FormLib, $naiyou_list;
	$htmldata['TITLE'] = "：確認";
	$htmldata['ACTION'] = "./?act=2";
	$htmldata['ACTION_CLASS'] = "confirm";
	$htmldata['READ_TEXT'] = '<p class="txt-conf">下記内容でよろしければ「送信する」ボタンを押してください。</p>';
	$err = array();

	// トリミング
	$post['name'] = $FormLib->mb_trim($post['name']);
	$post['name-kana'] = $FormLib->mb_trim($post['name-kana']);
	$post['organization'] = $FormLib->mb_trim($post['organization']);
	$post['address-level2'] = $FormLib->mb_trim($post['address-level2']);
	$post['address-line1'] = $FormLib->mb_trim($post['address-line1']);
	$post['tel'] = $FormLib->mb_trim($post['tel']);
	$post['fax'] = $FormLib->mb_trim($post['fax']);
	$post['email'] = $FormLib->mb_trim($post['email']);
	$post['naiyou'] = $FormLib->mb_trim($post['naiyou']);


	// エラーチェック
	if( strlen($post['name']) == 0) $err[] = "お名前を入力してください。";
	if( strlen($post['name-kana']) == 0) $err[] = "フリガナを入力してください。";
	if( strlen($post['tel']) == 0 ){$err[] = "電話番号を入力してください。";}
	if( strlen($post['email']) == 0 ){$err[] = "メールアドレスを入力してください。";}
	if(strlen($post['naiyou']) == 0 && count($post['naiyou_chk']) == 0){$err[] = "お問い合わせ内容を入力してください。";}

	// エラーあり
	if(!empty($err)) return input($err);

	// エラー無し
	$br_naiyou = nl2br($post['naiyou']);

	if(isset($post["naiyou_chk"])){
	  foreach((array) $post["naiyou_chk"] as $val){
	  	$naiyou_chk .= '<span class="naiyou_chk">'.$naiyou_list[$val-1].'<input type="hidden" name="naiyou_chk[]" value='.$val.'></span>';
	  }
	}

	$htmldata['FORM_VIEW'] = <<< HTML
	<dl class="name-box">
		<dt>お名前</dt>
		<dd>
			<div class="box">
				{$post['name']}<br>
				{$post['name-kana']}
				<input type="hidden" name="name" value="{$post['name']}">
				<input type="hidden" name="name-kana" value="{$post['name-kana']}">
			</div>
		</dd>
	</dl>
HTML;

if(strlen($post["organization"]) != 0){
$htmldata['FORM_VIEW'] .= <<< HTML
<dl class="organization-box">
	<dt>会社・店舗・団体名</dt>
	<dd>
		<div class="box">
			{$post['organization']}<input type="hidden" name="organization" value="{$post['organization']}">
		</div>
	</dd>
</dl>
HTML;
}

if(strlen($post["address-level1"].$post["address-level2"].$post["address-line1"]) != 0){
$htmldata['FORM_VIEW'] .= <<< HTML
<dl class="address-box">
	<dt>住所</dt>
	<dd>
		<div class="box">
				{$post['address-level1']}<input type="hidden" name="address-level1" value="{$post['address-level1']}">
				{$post['address-level2']}<input type="hidden" name="address-level2" value="{$post['address-level2']}">
				{$post['address-line1']}<input type="hidden" name="address-line1" value="{$post['address-line1']}">
		</div>
	</dd>
</dl>
HTML;
}

if(strlen($post["tel"]) != 0){
$htmldata['FORM_VIEW'] .= <<< HTML
<dl class="tel-box">
	<dt>電話番号</dt>
	<dd>
		<div class="box">
			{$post['tel']}<input type="hidden" name="tel" value="{$post['tel']}">
		</div>
	</dd>
</dl>
HTML;
}

if(strlen($post["fax"]) != 0){
$htmldata['FORM_VIEW'] .= <<< HTML
<dl class="fax-box">
	<dt>FAX番号</dt>
	<dd>
		<div class="box">
			{$post['fax']}<input type="hidden" name="fax" value="{$post['fax']}">
		</div>
	</dd>
</dl>
HTML;
}

$htmldata['FORM_VIEW'] .= <<< HTML
	<dl class="email-box">
		<dt>メールアドレス</dt>
		<dd>
			<div class="box">
				{$post['email']}<input type="hidden" name="email" value="{$post['email']}">
			</div>
		</dd>
	</dl>
	<dl class="naiyou-box">
		<dt>お問い合わせ内容</dt>
		<dd>
			<div class="box">
				<div>{$naiyou_chk}</div>
				<div>{$br_naiyou}</div>
				<input type="hidden" name="naiyou" value="{$post['naiyou']}">
			</div>
		</dd>
	</dl>
	<div class="btn-box mo-btn-1">
		<input type="hidden" name="submitback" id="submitback" value="">
		<a href="javascript:void(0)" class="btn2 back">
			修正
		</a>
		<a href="javascript:form1.submit()" class="btn2">
			送信する
		</a>
	</div>
HTML;
}

function input($err)
{
	global $htmldata, $FormLib, $post, $param, $naiyou_list, $txt_list;

	for($i=1;$i<=count($naiyou_list);$i++){
		$checked["naiyou_chk"][$i]="";
	}
	if(isset($post["naiyou_chk"])){
	  foreach((array) $post["naiyou_chk"] as $val){
	    $checked["naiyou_chk"][$val] = " checked";
	  }
	}
	$naiyou_count = 0;
	$naiyou_list_tag = "";
	foreach ($naiyou_list as $val) {
		$naiyou_list_tag .= '<label><input type="checkbox" name="naiyou_chk[]" value="'.($naiyou_count+1).'" '.$checked["naiyou_chk"][$naiyou_count+1].'>'.$naiyou_list[$naiyou_count].'</label>';
		$naiyou_count++;
	}


	$htmldata['TITLE'] = "";
	$htmldata['ACTION'] = "./?act=1";
	$htmldata['ACTION_CLASS'] = "input";
	$htmldata['READ_TEXT']  = <<< HTML
<div class="read">
  <p>{$txt_list[0]}<br>お問い合わせは、通常3営業日以内に担当者よりご連絡いたします。<br>内容によって回答にお時間がかかる場合があります。あらかじめご了承ください。</p>
</div>
HTML;
	$htmldata['ERR_TEXT'] = $FormLib->setErrText($err);
	$htmldata['FORM_VIEW'] = <<< HTML
	<dl class="name-box hissu">
		<dt><span>お名前</span></dt>
		<dd>
			<div class="box">
				<input type="text" name="name" value="{$post['name']}" placeholder="田中 一郎" required>
			</div>
			<div class="box ">
				<input type="text" name="name-kana" value="{$post['name-kana']}" placeholder="タナカ イチロウ" required>
			</div>
		</dd>
	</dl>
	<dl class="organization-box">
		<dt><span>会社・店舗・団体名</span></dt>
		<dd>
			<div class="box">
				<input type="text" name="organization" value="{$post['organization']}" placeholder="Takuya kanpo consulting">
			</div>
		</dd>
	</dl>

	<dl class="address-box">
		<dt><span>住所</span></dt>
		<dd>
			<div class="box">
				<select name="address-level1">
					<option value="">都道府県</option>
					<option value="北海道">北海道</option>
					<option value="青森県">青森県</option>
					<option value="岩手県">岩手県</option>
					<option value="宮城県">宮城県</option>
					<option value="秋田県">秋田県</option>
					<option value="山形県">山形県</option>
					<option value="福島県">福島県</option>
					<option value="茨城県">茨城県</option>
					<option value="栃木県">栃木県</option>
					<option value="群馬県">群馬県</option>
					<option value="新潟県">新潟県</option>
					<option value="山梨県">山梨県</option>
					<option value="長野県">長野県</option>
					<option value="埼玉県">埼玉県</option>
					<option value="千葉県">千葉県</option>
					<option value="東京都">東京都</option>
					<option value="神奈川県">神奈川県</option>
					<option value="富山県">富山県</option>
					<option value="石川県">石川県</option>
					<option value="福井県">福井県</option>
					<option value="岐阜県">岐阜県</option>
					<option value="静岡県">静岡県</option>
					<option value="愛知県">愛知県</option>
					<option value="三重県">三重県</option>
					<option value="滋賀県">滋賀県</option>
					<option value="京都府">京都府</option>
					<option value="大阪府">大阪府</option>
					<option value="兵庫県">兵庫県</option>
					<option value="奈良県">奈良県</option>
					<option value="和歌山県">和歌山県</option>
					<option value="鳥取県">鳥取県</option>
					<option value="島根県">島根県</option>
					<option value="岡山県">岡山県</option>
					<option value="広島県">広島県</option>
					<option value="山口県">山口県</option>
					<option value="徳島県">徳島県</option>
					<option value="香川県">香川県</option>
					<option value="愛媛県">愛媛県</option>
					<option value="高知県">高知県</option>
					<option value="福岡県">福岡県</option>
					<option value="佐賀県">佐賀県</option>
					<option value="長崎県">長崎県</option>
					<option value="熊本県">熊本県</option>
					<option value="大分県">大分県</option>
					<option value="宮崎県">宮崎県</option>
					<option value="鹿児島県">鹿児島県</option>
					<option value="沖縄県">沖縄県</option>
				</select>
			</div>
			<div class="box">
				<input type="text" name="address-level2" value="{$post['address-level2']}" placeholder="座間市">
				<input type="text" name="address-line1" value="{$post['address-line1']}" placeholder="相模が丘5-10-37">
			</div>
		</dd>
	</dl>
	<dl class="tel-box hissu">
		<dt><span>電話番号</span></dt>
		<dd>
			<div class="box">
				<input type="text" name="tel" value="{$post['tel']}" placeholder="042-746-1951" required>
			</div>
		</dd>
	</dl>
	<dl class="fax-box">
		<dt><span>FAX番号</span></dt>
		<dd>
			<div class="box">
				<input type="text" name="fax" value="{$post['fax']}" placeholder="042-746-1951">
			</div>
		</dd>
	</dl>
	<dl class="email-box hissu">
		<dt><span>メールアドレス</span></dt>
		<dd>
			<div class="box">
				<input type="text" name="email" value="{$post['email']}" placeholder="info@takuya-kanpo-consulting.com" required>
			</div>
		</dd>
	</dl>
	<dl class="naiyou-box hissu">
		<dt><span>お問い合わせ内容</span></dt>
		<dd>
			<div class="box">
				<div class="naiyou-box-in">
					{$naiyou_list_tag}
				</div>
				<div class="naiyou-box-in">
					<textarea name="naiyou" rows="15" cols="50" placeholder="お問い合わせ内容をご記入ください。">{$post['naiyou']}</textarea>
				</div>
			</div>
		</dd>
	</dl>
	<div class="btn-box mo-btn-1">
		<a href="javascript:form1.submit()">内容を確認する</a>
	</div>
HTML;
}

// HTML出力
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
  <title>お問い合わせ | Takuya kanpo consulting</title>
  <meta property="og:type" content="article">
  <meta name="description" content="新宿にて毎月セミナー開催中です！">
<?php readfile('../assets/ssi/head.html'); ?>
</head>
<body class="under greeting">
  <div id="wrapper">
<?php readfile('../assets/ssi/header.html'); ?>
  	<div id="contents" class="under-inner <?php echo $htmldata['ACTION_CLASS']; ?>">
      <header class="text-header">
        <h1><span>お問い合わせ</span><span>Contact</span></h1>
      </header>
			<section class="mailform">
<?php echo $htmldata['READ_TEXT']; ?>
				<form id="form1" name="form1" action="<?php echo $htmldata['ACTION']; ?>" method="post">
<?php echo $htmldata['ERR_TEXT']; ?>
<?php echo $htmldata['FORM_VIEW']; ?>
				</form>
			</section>
    </div>
  </div>
<?php readfile('../assets/ssi/footer.html'); ?>
<script src="/assets/js/min/mailform.min.js"></script>
<?php
if(DEBUG) echo '<p style="position: fixed; top: 0px; left: 0px; color: #fff; font-weight: bold; font-size: 20px; z-index: 9999; background-color: #ff0000; padding: 1rem;">テスト中です。</p>';
?>
</body>
</html>

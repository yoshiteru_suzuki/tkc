function sendMailGoogleForm_20190414() {
	//自動返信メールの件名
	var title = "[Takuya kanpo consulting] タクヤ先生＆櫻井店長コラボセミナー 2019.4.14 お申し込み（自動返信メール）";

	//自動返信メールの本文1(\nは改行)
	var body2;
	var body
	= "この度はお申し込みをいただき、誠にありがとうございます。\n"
	+ "\n"
	+ "【お振込から当日までの手順】\n\n"
	+ "①以下の指定口座へ受講料をお振込下さい。\n"
	+ "楽天銀行 第二営業支店\n"
	+ "普通 7642216\n"
	+ "合同会社　Takuya kanpo consulting\n"
	+ "\n"
	+ "②連絡用Line@にご登録下さい。\n"
	+ "https://line.me/R/ti/p/%40uka1348d\n"
	+ "※Lineをお使いでない場合は\n"
	+ "takuya0613@gmail.com が連絡用のメールアドレスになりますので必ずご登録下さい。\n"
	+ "\n"
	+ "③ご登録いただきました連絡ツールより「申込み名」と「お振込完了」の旨をご連絡下さい。確認後参加登録完了となります。\n"
	+ "ご連絡がない場合はお振込みの確認が遅れる場合がありますのでご注意下さい。\n"
	+ "\n-----------------\n\n"
	+ "【キャンセルポリシー】\n\n"
	+ "お振込後のキャンセルにつきましては、いかなる事情でもキャンセルやご欠席によるご返金は出来かねますのでご了承下さい。\n"
	+ "\n-----------------\n\n"
	+ "【セミナー会場】\n\n"
	+ "AP西新宿東京都新宿区西新宿7丁目2番4号 新宿喜楓ビル5F\n"
	+ "https://www.tc-forum.co.jp/kanto-area/ap-nishishinjuku/ni-base/\n"
	+ "\n"
	+ "開場は講義30分前となります。遅刻、欠席につきましては必ずご連絡下さい。\n"
	+ "当日緊急連絡先：080-3300-3719となります。\n"
	+ "\n-----------------\n\n"
	+ "【欠席について】\n\n"
	+ "やむを得ず欠席された場合は動画配信が可能となりますので、必ず参加日までに欠席連絡をお願いいたします。無断欠席の場合は配信をお断りさせていただきます。\n"
	+ "\n-----------------\n\n"
	+ "【その他の注意事項】\n\n"
	+ "※主催側の何らかの都合でセミナーの途中終了が起こった場合はセミナー費は全額返金させていただきます。\n"
	+ "※セミナー中に参加者、講師への誹謗中傷、及び適切でない参加姿勢が繰り返しあった場合、途中退室をしていただくことがあります。\n"
	+ "※本セミナーに対するご質問等は前述のLINE、gmailへお問い合わせ下さい。\n"
	+ "※領収書のご入用の方は当日にお申し付け下さい。\n\n"
	+ "\n-----------------\n\n"
	+ "【お申し込み内容】\n\n";
 	//本文作成用の変数
	var sheet = SpreadsheetApp.getActiveSheet();
	var row = sheet.getLastRow();
	var column = sheet.getLastColumn();
	var range = sheet.getDataRange();

	var  mail = "";
	var  name = "";

	for (var i = 1; i <= column; i++ ) {
		//スプレッドシートの入力項目名を取得
		var header = range.getCell(1, i).getValue();
		//スプレッドシートの入力値を取得
		var value = range.getCell(row, i).getValue();

		if ( header === 'タイムスタンプ' ) {
			continue;
		}
		if ( header === 'お名前' ) {
			name += value+" 様\n\n";
		}

		//本文1(body)にスプレッドシートの入力項目を追加
		body += "■"+header+"\n";
		body += value + "\n\n";


		//フォームの入力項目が「メールアドレス」の場合は、変数mailに代入
 		if ( header === 'メールアドレス' ) {
   		mail = value;
 		}
 	}

	body += "------------------------------------------------------\n"
	body += "Takuya kanpo consulting\n"
	body += "http://takuya-kanpo-consulting.com/\n"


	body2 = name+body;

	//宛名＝mail、件名＝title、本文=bodyで、メールを送る
	GmailApp.sendEmail(mail,title,body2);
}

function sendMailGoogleForm_tkc_seminar_20191201(event) {
	var itemResponses = event.response.getItemResponses();
	var address = event.response.getRespondentEmail();

	//自動返信メールの件名
	var title = "[Takuya kanpo consulting] 2019.12.1 中医学を活かして必ず起業できるセミナー お申し込み（自動返信メール）";

	//自動返信メールの本文1(\nは改行)
	var name;
	var body2;
	var body
	= "この度はお申し込みをいただき、誠にありがとうございます。\n"
	+ "\n"
	+ "【お振込から当日までの手順】\n\n"
	+ "①お申込みから必ず1週間以内に以下の指定口座へ受講料33,000円をお振込下さい。\n"
	+ "楽天銀行 第二営業支店\n"
	+ "普通 7642216\n"
	+ "合同会社　Takuya kanpo consulting\n"
	+ "※1週間以内のお振込が確認できない場合、先着順で10名が埋まってしまった際にはお申し込みをキャンセルさせていただきますのでご了承下さい。\n"
	+ "\n"
	+ "②連絡用Line@にご登録下さい。\n"
	+ "https://line.me/R/ti/p/%40380ilgyx\n"
	+ "※Lineをお使いでない場合は\n"
	+ "takuya0613@gmail.com が連絡用のメールアドレスになりますので必ずご登録下さい。\n"
	+ "\n"
	+ "③ご登録いただきました連絡ツールより「お申込み名」と「お振込完了」の旨をご連絡下さい。確認後参加登録完了となります。\n"
	+ "ご連絡がない場合はお振込みの確認が遅れる場合がありますのでご注意下さい。\n"
	+ "\n-----------------\n\n"
	+ "【キャンセルポリシー】\n\n"
	+ "お振込後のキャンセルにつきましては開催者の都合によるキャンセル、日程変更を除き、いかなる事情でもキャンセルやご欠席によるご返金は出来かねますのでご了承下さい。\n"
	+ "\n-----------------\n\n"
	+ "【開催地】\n\n"
	+ "新宿三丁目会議室\n"
	+ "http://www.shinjuku-kaigi.com/access.html\n"
	+ "\n"
	+ "開場は講義15分前となります。遅刻、欠席につきましては必ずご連絡下さい。\n"
	+ "当日緊急連絡先：080-3300-3719となります。\n"
	+ "\n-----------------\n\n"
	+ "【その他の注意事項】\n\n"
	+ "※主催側の何らかの都合でセミナー開催中止が起こった場合はセミナー費は全額返金させていただきます。\n"
	+ "※セミナー参加中に参加者、講師への誹謗中傷、及び適切でない参加姿勢が繰り返しあった場合、その後の参加をお断りすることがございます。\n"
	+ "※本セミナーに対するご質問等は前述のLINE、gmailへお問い合わせ下さい。\n"
	+ "※領収書のご入用の方は当日までにお申し付け下さい。\n"
	+ "\n-----------------\n\n"
	+ "【お申し込み内容】\n\n";

	for (var i = 0; i < itemResponses.length; i++) {
		var itemResponse = itemResponses[i];
		var question = itemResponse.getItem().getTitle();
		// var type = itemResponse.getItem().getType();
		var answer = itemResponse.getResponse();


		if ( question === 'タイムスタンプ' ) {
			continue;
		}
		// if ( question === '参加開始日' ) {
		// 	if(answer){
		// 		answer = Utilities.formatDate(answer,"JST","yyyy/MM/dd");
		// 	}
		// }
		if ( question === 'お名前' ) {
			name = answer+" 様\n\n";
		}

		//本文1(body)にスプレッドシートの入力項目を追加
		body += "■"+question+"\n";
		body += answer + "\n\n";


	}

	body += "\n-----------------\n\n";
	body += "皆様とお会い出来ることを心より楽しみにしております。\n";
	body += "\n";
	body += "合同会社　Takuya kanpo consulting\n";
	body += "合同代表・講師 杉山卓也\n";


	body2 = name+body;

	//宛名＝mail、件名＝title、本文=bodyで、メールを送る
	GmailApp.sendEmail(address,title,body2);
}

function sendMailGoogleForm_tkc_seminar_20191222(event) {
	var itemResponses = event.response.getItemResponses();
	var address = event.response.getRespondentEmail();

	//自動返信メールの件名
	var title = "[Takuya kanpo consulting] 2019.12.22 タクヤ先生＆櫻井店長コラボセミナーお申し込み（自動返信メール）";

	//自動返信メールの本文1(\nは改行)
	var name;
	var body2;
	var body
	= "この度はお申し込みをいただき、誠にありがとうございます。\n"
	+ "\n"
	+ "【お振込から当日までの手順】\n\n"
	+ "①申込みから必ず１週間以内に以下の指定口座へ受講料のみをお振込下さい。\n"
	+ "【重要】受講料以外（書籍・薬膳セット）は当日現地で現金でのお支払いとなります！\n"
	+ "お申し込みから１週間以内にお振込みがない場合はお申し込みを取り消しさせていただきます。次回以降のお申込みをお断りすることもありますのでどうか厳守下さい。\n"
	+ "楽天銀行 第二営業支店\n"
	+ "普通 7642216\n"
	+ "合同会社　Takuya kanpo consulting\n"
	+ "\n"
	+ "②連絡用Line@にご登録下さい。\n"
	+ "https://line.me/R/ti/p/%40uka1348d\n"
	+ "※Lineをお使いでない場合は\n"
	+ "takuya0613@gmail.com が連絡用のメールアドレスになりますので必ずご登録下さい。\n"
	+ "\n"
	+ "③ご登録いただきました連絡ツールより「申込み名」と「お振込完了」の旨をご連絡下さい。確認後参加登録完了となります。\n"
	+ "ご連絡がない場合はお振込みの確認が遅れる場合がありますのでご注意下さい。\n"
	+ "\n-----------------\n\n"
	+ "【キャンセルポリシー】\n\n"
	+ "お振込後のキャンセルにつきましては、いかなる事情でもキャンセルやご欠席によるご返金は出来かねますのでご了承下さい。\n"
	+ "\n-----------------\n\n"
	+ "【セミナー会場】\n\n"
	+ "AP西新宿\n東京都新宿区西新宿7丁目2番4号 新宿喜楓ビル5F\n"
	+ "https://www.tc-forum.co.jp/kanto-area/ap-nishishinjuku/ni-base/\n"
	+ "\n"
	+ "開場は講義30分前となります。遅刻、欠席につきましては必ずご連絡下さい。\n"
	+ "当日緊急連絡先：080-3300-3719となります。\n"
	+ "\n-----------------\n\n"
	+ "【動画配信】\n\n"
	+ "動画配信はセミナー終了後1週間程度後となります。\n"
	+ "お申し込みいただきましたこちらのメールアドレスに資料データ添付と共に配信させていただきます。\n"
	+ "※オンタイム配信ではありませんのでご注意ください。\n"
	+ "\n-----------------\n\n"
	+ "【欠席について】\n\n"
	+ "やむを得ず欠席された場合は動画配信が可能となりますので、必ず参加日までに欠席連絡をお願いいたします。無断欠席の場合は配信をお断りさせていただきます。\n"
	+ "\n-----------------\n\n"
	+ "【その他の注意事項】\n\n"
	+ "※主催側の何らかの都合でセミナーの途中終了が起こった場合はセミナー費は全額返金させていただきます。\n"
	+ "※セミナー中に参加者、講師への誹謗中傷、及び適切でない参加姿勢が繰り返しあった場合、途中退室をしていただくことがあります。\n"
	+ "※本セミナーに対するご質問等は前述のLINE、gmailへお問い合わせ下さい。\n"
	+ "\n-----------------\n\n"
	+ "【お申し込み内容】\n\n"

	for (var i = 0; i < itemResponses.length; i++) {
		var itemResponse = itemResponses[i];
		var question = itemResponse.getItem().getTitle();
		// var type = itemResponse.getItem().getType();
		var answer = itemResponse.getResponse();


		if ( question === 'タイムスタンプ' ) {
			continue;
		}
		// if ( question === '参加開始日' ) {
		// 	if(answer){
		// 		answer = Utilities.formatDate(answer,"JST","yyyy/MM/dd");
		// 	}
		// }
		if ( question === 'お名前' ) {
			name = answer+" 様\n\n";
		}

		//本文1(body)にスプレッドシートの入力項目を追加
		body += "■"+question+"\n";
		body += answer + "\n\n";


	}

	body += "\n-----------------\n\n"
	body += "皆様とお会い出来ることを心より楽しみにしております。\n\n"
	body += "このメールは配信専用です。\nご連絡はtakuya0613@gmail.comまでお願いいたします。\n"
	body += "\n-----------------\n\n"
	body += "Takuya kanpo consulting\n"
	body += "https://takuya-kanpo-consulting.com/\n\n"
	body += "（漢方のスギヤマ薬局 本店内）\n"
	body += "〒 252-0001 座間市相模が丘5-10-37\n"
	body += "TEL: 042-746-1951\n"


	body2 = name+body;

	//宛名＝mail、件名＝title、本文=bodyで、メールを送る
	GmailApp.sendEmail(address,title,body2);
}
